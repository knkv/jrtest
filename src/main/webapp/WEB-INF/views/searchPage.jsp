<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${title}</title>
</head>
<body>

<h1>Search User</h1>

<c:url var="searchUrl" value="/users/search" />

<c:forEach var="message" items="${messages}">
    <li>${message}</li>
</c:forEach>

<form:form modelAttribute="userAttribute" method="POST" action="${searchUrl}">

    <table>
        <tr>
            <td><form:label path="name">Name:</form:label></td>
            <td><form:input path="name"/></td>
        </tr>
        <tr>
            <td><form:label path="age">Age</form:label></td>
            <td><form:input path="age"/></td>
        </tr>
    </table>

    <input type="submit" value="Search" />

</form:form>

</body>
</html>
