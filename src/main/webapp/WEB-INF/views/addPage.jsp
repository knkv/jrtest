<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${title}</title>
</head>
<body>

<h1>Create New User</h1>

<c:url var="saveUrl" value="/users/add" />

<c:forEach var="message" items="${messages}">
    <li>${message}</li>
</c:forEach>

<form:form modelAttribute="userAttribute" method="POST" action="${saveUrl}">

    <table>
        <tr>
            <td><form:label path="name">Name:</form:label></td>
            <td><form:input path="name"/></td>
        </tr>

        <tr>
            <td><form:label path="age">Age</form:label></td>
            <td><form:input path="age"/></td>
        </tr>

        <tr>
            <td><form:label path="admin">Admin</form:label></td>
            <td>
                <select name='admin'>
                    <option value="true" selected=''>yes</option>
                    <option value="false" selected='selected'>no</option>
                </select>
            </td>
        </tr>
    </table>

    <input type="submit" value="Save" />
</form:form>

</body>
</html>
