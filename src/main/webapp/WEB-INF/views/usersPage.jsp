<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>${title}</title>
</head>
<body>

<c:set var="users" scope="session" value="${users}"/>
<c:set var="totalCount" scope="session" value="${users.size()}"/>
<c:set var="perPage" scope="session"  value="${5}"/>
<c:set var="pageStart" value="${param.start}"/>

<c:if test="${empty pageStart or pageStart < 0}">
    <c:set var="pageStart" value="0"/>
</c:if>

<c:if test="${totalCount < pageStart}">
    <c:set var="pageStart" value="${pageStart - perPage}"/>
</c:if>



<h1>Users</h1>
<c:url var="mainUrl" value="/users"/>
<c:url var="addUserUrl" value="/users/add"/>
<c:url var="editUserUrl" value="/users/edit"/>
<c:url var="deleteUserUrl" value="/users/delete"/>
<c:url var="searchUserUrl" value="/users/search"/>

<a href="${mainUrl}">Main Page</a>
<br>
<a href="${addUserUrl}">Add New User</a>
<br>
<a href="${searchUserUrl}">Search User</a>
<br>
<p>User count: ${totalCount}</p>
<table border="1">
    <tr>
        <td>User Id</td>
        <td>User Name</td>
        <td>User Age</td>
        <td>Administrator</td>
        <td>Created Date</td>
    </tr>

    <c:forEach var="user" items="${users}" begin="${pageStart}" end="${pageStart + perPage - 1}">
        <tr>
            <td>${user.id}</td>
            <td>${user.name}</td>
            <td>${user.age}</td>
            <td>${user.admin == true ? "yes" : "no"}</td>
            <td>${user.createdDate}</td>
            <td><a href="${editUserUrl}?id=${user.id}">Edit</a></td>
            <td><a href="${deleteUserUrl}?id=${user.id}">Delete</a></td>
        </tr>
    </c:forEach>

</table>

<a href="?start=${pageStart - perPage}">Previous (</a>${pageStart + 1} - ${pageStart + perPage}
<a href="?start=${pageStart + perPage}">) Next</a>

</body>
</html>