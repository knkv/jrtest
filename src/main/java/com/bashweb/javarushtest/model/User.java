package com.bashweb.javarushtest.model;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

//@Component
@Entity
@Table(name = "user")
public class User implements Serializable{

    private static final long serialVersionUID = -5527566248002296042L;

    public User() {
    }

    @Id
    @Column(name = "id", length = 8)
    @GeneratedValue
    private Integer id;

    @Column(name = "name", length = 25)
    private String name;

    @Column(name = "age")
    private Integer age;

    @Column(name = "isAdmin")
    private boolean admin;

    @Column(name = "createdDate")
    @CreationTimestamp
    private Timestamp createdDate;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public Timestamp getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Timestamp createdDate) {
        this.createdDate = createdDate;
    }
}
