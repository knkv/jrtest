package com.bashweb.javarushtest.controller;

import com.bashweb.javarushtest.model.User;
import com.bashweb.javarushtest.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping(value = {"/", "/users"})
public class UserController {

    //temp
    UserService userService = new UserService();

    @ModelAttribute
    public void addCommonObjects(Model model){
        model.addAttribute("title", "JavaRush Test Project");
    }

	@RequestMapping(method = RequestMethod.GET)
	public String getUsers(Model model) {
        List<User> users = userService.getAll();
		model.addAttribute("users", users);
		return "usersPage";
	}

	@RequestMapping(value = "/users/add", method = RequestMethod.GET)
	public String getAddUserPage(Model model) {
        model.addAttribute("userAttribute", new User());
		return "addPage";
	}

    @RequestMapping(value = "/users/add", method = RequestMethod.POST)
    public String getAddedUserPage(Model model, @ModelAttribute("userAttribute") User createdUser) {
        List<String> messageList = new ArrayList<>();

        if (createdUser.getName().isEmpty()){
            messageList.add("Please fill username field.");
        }
        if (createdUser.getAge() == null){
            messageList.add("Please fill age field.");
        }

        if (messageList.size() != 0){
            model.addAttribute("messages", messageList);
            return "addPage";
        }
        userService.add(createdUser);
        model.addAttribute("createdUser", createdUser);
        return "addedPage";
    }

    @RequestMapping(value = "/users/edit", method = RequestMethod.GET)
    public String getEditUserPage(@RequestParam(value="id", required=true) Integer id, Model model) {
        User user = userService.get(id);
        model.addAttribute("user", user);
        return "editPage";
    }

    @RequestMapping(value = "/users/edit", method = RequestMethod.POST)
    public String getEditedUserPage(Model model, @ModelAttribute("editedUser") User editedUser) {
        List<String> messageList = new ArrayList<>();

        if (editedUser.getName().isEmpty()){
            messageList.add("Don't leave name field empty.");
        }
        if (editedUser.getAge() == null){
            messageList.add("Don't leave age field empty.");
        }
        if (messageList.size() != 0){
            model.addAttribute("messages", messageList);
            User user = userService.get(editedUser.getId());
            model.addAttribute("user", user);
            return "editPage";
        }
        userService.edit(editedUser);
        model.addAttribute("editedUser", editedUser);
        return "editedPage";
    }

    @RequestMapping(value = "/users/delete", method = RequestMethod.GET)
    public String getDeleteUserPage(@RequestParam(value="id", required=true) Integer id, Model model) {
        User user = userService.get(id);
        model.addAttribute("user", user);
        return "deletePage";
    }

    @RequestMapping(value = "/users/deleted", method = RequestMethod.GET)
    public String getDeletedUserPage(@RequestParam(value="id", required=true) Integer id, Model model) {
        model.addAttribute("text", "User deleted");
        userService.delete(id);
        return "deletedPage";
    }

    @RequestMapping(value = "/users/search", method = RequestMethod.GET)
    public String getUserSearchPage(Model model) {
        model.addAttribute("userAttribute", new User());
        return "searchPage";
    }

    @RequestMapping(value = "/users/search", method = RequestMethod.POST)
    public String getUserResultPage(Model model, @ModelAttribute("editedUser") User searchedUser) {

        List<User> users = userService.search(searchedUser.getName(), searchedUser.getAge());
        model.addAttribute("users", users);
        return "resultPage";
    }
}