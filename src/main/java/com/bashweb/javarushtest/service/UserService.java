package com.bashweb.javarushtest.service;

import com.bashweb.javarushtest.model.User;
import com.bashweb.javarushtest.utils.HibernateUtil;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service("userService")
@Transactional
public class UserService {

    private static final Logger logger = LogManager.getLogger(UserService.class);

    public List<User> getAll(){
        logger.debug("Retrieving all users.");
        Session session = HibernateUtil.getSessionFactory().openSession();
        Query query = session.createQuery("from User");
        return query.list();
    }

    public User get(Integer id) {
        logger.debug(String.format("Retrieving user with id: %d.", id));
        Session session = HibernateUtil.getSessionFactory().openSession();
        return (User) session.get(User.class, id);
    }

    public void add(User user) {
        logger.debug("Adding new user.");
        Session session = HibernateUtil.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(user);
        session.getTransaction().commit();
        session.close();
    }

    public void delete(Integer id) {
        logger.debug("Deleting existing user.");
        Session session = HibernateUtil.getSessionFactory().openSession();
        User user = (User) session.get(User.class, id);

        session.beginTransaction();
        session.delete(user);
        session.getTransaction().commit();
        session.close();
    }

    public void edit(User user) {
        logger.debug("Editing existing user.");
        Session session = HibernateUtil.getSessionFactory().openSession();
        User existingUser = (User) session.get(User.class, user.getId());

        existingUser.setName(user.getName());
        existingUser.setAge(user.getAge());
        existingUser.setAdmin(user.isAdmin());
        existingUser.setCreatedDate(user.getCreatedDate());

        session.beginTransaction();
        session.save(existingUser);
        session.getTransaction().commit();
        session.close();
    }

    // TODO: to be refactored (add build)
    public List<User> search(String name, Integer age){
        List<User> foundUsers;
        Session session = HibernateUtil.getSessionFactory().openSession();

        String whereName = !name.isEmpty() ? String.format(" and U.name = '%s' ", name) : "";
        String whereAge = age != null  ? " and U.age = " + age : "";

        String queryString = String.format("from User U where 1=1 %s %s", whereName, whereAge);
        Query query = session.createQuery(queryString);

        foundUsers = (List<User>)query.list();
        return foundUsers;
    }
}
