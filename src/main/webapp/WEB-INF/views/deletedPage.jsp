<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${title}</title>
</head>
<body>


<h1>Deleted User Info</h1>

<c:url var="mainUrl" value="/users" />

<p>${text}</p>

<a href="${mainUrl}">Navigate to Users Page</a>

</body>
</html>
