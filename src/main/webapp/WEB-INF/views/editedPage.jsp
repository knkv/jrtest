<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>

<h1>Edited User Info</h1>

<c:url var="mainUrl" value="/users" />

<table border="1">
    <p>User Id: ${editedUser.id}</p>
    <p>User Name: ${editedUser.name}</p>
    <p>User Age: ${editedUser.age}</p>
    <p>Administrator: ${editedUser.admin == true ? "yes" : "no"}</p>
    <p>Created Date: ${editedUser.createdDate}</p>
</table>

<a href="${mainUrl}">Navigate to Users Page</a>

</body>
</html>
