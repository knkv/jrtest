<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>${title}</title>
</head>
<body>

<c:set var="totalCount" scope="session" value="${users.size()}"/>

<h1>Found Users</h1>
<c:url var="mainUrl" value="/users"/>

<a href="${mainUrl}">Main Page</a>
<br>
<p>Found user count: ${totalCount}</p>
<table border="1">
    <tr>
        <td>User Id</td>
        <td>User Name</td>
        <td>User Age</td>
        <td>Administrator</td>
        <td>Created Date</td>
    </tr>

    <c:forEach var="user" items="${users}">
        <tr>
            <td>${user.id}</td>
            <td>${user.name}</td>
            <td>${user.age}</td>
            <td>${user.admin == true ? "yes" : "no"}</td>
            <td>${user.createdDate}</td>
            <td><a href="${editUserUrl}?id=${user.id}">Edit</a></td>
            <td><a href="${deleteUserUrl}?id=${user.id}">Delete</a></td>
        </tr>
    </c:forEach>

</table>


</body>
</html>