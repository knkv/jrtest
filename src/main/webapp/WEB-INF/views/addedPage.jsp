<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${title}</title>
</head>
<body>

<h1>Added User Info</h1>

<c:url var="mainUrl" value="/users" />

<table border="1">
    <p>User Id: ${createdUser.id}</p>
    <p>User Name: ${createdUser.name}</p>
    <p>User Age: ${createdUser.age}</p>
    <p>Administrator: ${createdUser.admin == true ? "yes" : "no"}</p>
    <p>Created Date: ${createdUser.createdDate}</p>
</table>

<a href="${mainUrl}">Navigate to Users Page</a>

</body>
</html>
