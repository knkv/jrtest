<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${title}</title>
</head>
<body>

<h1>Delete User</h1>

<c:url var="deleteUrl" value="/users/deleted" />
<c:url var="cancelUrl" value="/users" />

<h2>Are you sure to delete the user?</h2>
<p>Id: ${user.id}</p>
<p>Name: ${user.name}</p>
<p>Age: ${user.age}</p>
<p>Is admin: ${user.admin}</p>

<p><a href="${deleteUrl}?id=${user.id}">I'm 146% sure</a></p>
<p><a href="${cancelUrl}">NO</a></p>

</body>
</html>
