<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>${title}</title>
</head>
<body>

<h1>Edit User</h1>

<c:url var="saveUrl" value="/users/edit" />
<c:url var="cancelUrl" value="/users" />

<c:forEach var="message" items="${messages}">
    <li>${message}</li>
</c:forEach>

<form:form modelAttribute="user" method="POST" action="${saveUrl}">

    <input type="hidden" id="${user.id}" value="${user.id}" />
    <input type="hidden" id="user.createdDate" value="${user.createdDate}" />

    <table>
        <tr>
            <td><form:label path="name">Name:</form:label></td>
            <td><form:input path="name"/></td>
        </tr>

        <tr>
            <td><form:label path="age">Age</form:label></td>
            <td><form:input path="age"/></td>
        </tr>

        <tr>
            <td><form:label path="admin">Admin</form:label></td>
            <td>
                <select name="admin">
                    <option value="true" ${user.admin == true ? "selected='selected'" : ""}>yes</option>
                    <option value="false" ${user.admin == false ? "selected='selected'" : ""}>no</option>
                </select>
            </td>
        </tr>
    </table>

    <input type="submit" value="Save" />
    <p><a href="${cancelUrl}">Cancel</a></p>

    <form:input type="hidden" path="id" value="${id}"/>
    <form:input type="hidden" path="createdDate" value="${createdDate}"/>

</form:form>



</body>
</html>